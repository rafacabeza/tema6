<?php

function deliverResponse($status, $status_message, $data)
{
    header("HTTP/1.1 $status $status_message");    
    $response['status']=$status;
    $response['status_message']=$status_message;
    $response['data']=$data;
    
    $jsonResponse = json_encode($response);
    echo $jsonResponse;
}

//error_reporting(E_ERROR );        

$data['request'] = $_GET['request'];
$data['method'] = $_SERVER['REQUEST_METHOD'];
$data['datos'] = $_POST;

if(!isset($_SERVER['REQUEST_METHOD'])){
    $method = 'GET';
}
else{
    $method = $_SERVER['REQUEST_METHOD'];
}

switch ($_SERVER['REQUEST_METHOD']){
case 'GET':
    $data['tarea']  = 'devolver lista de usuarios';
    break;
case 'POST':
    $data['tarea']  = 'añadir registro nuevo';
    break;
case 'PUT':
    $data['tarea']  = 'modificar registro';
    break;
case 'DELETE':
    $data['tarea']  = 'borrar registro';
    break;
default :
    $data['tarea']  = 'mensaje de error ';
    break;
}

deliverResponse(200, 'ok', $data);

